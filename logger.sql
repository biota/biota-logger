--
-- SPDX-License-Identifier: GPL-3.0-or-later
--
-- biota/biota-logger/logger.sql
-- Copyright (C) 2022 Fifth Estate
-- See biota:logger:db(8) for documentation
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
-- 
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
-- details.
-- 
-- You should have received a copy of the GNU General Public License along with
-- this program.  If not, see <https://www.gnu.org/licenses/>.
--


-- Biota schema
CREATE SCHEMA IF NOT EXISTS biota;


-- Reference table of individual sensors
CREATE TABLE IF NOT EXISTS biota.sensors (
    id		SERIAL,
    name	TEXT UNIQUE NOT NULL,
    location	TEXT,
    notes	TEXT,
    PRIMARY KEY	(id)
);


-- Reference table of captured metrics
CREATE TABLE IF NOT EXISTS biota.metrics (
    id		SERIAL,
    name	TEXT UNIQUE NOT NULL,
    unit	TEXT NOT NULL,
    notes	TEXT,
    PRIMARY KEY	(id)
);


-- Transaction table of telemetry readings
CREATE TABLE IF NOT exists biota.telemetry (
    id		 BIGSERIAL,
    sensor_id    INTEGER NOT NULL,
    metric_id	 INTEGER NOT NULL,
    reading 	 FLOAT NOT NULL,
    logged_at	 TIMESTAMPTZ NOT NULL DEFAULT now(),
    PRIMARY KEY  (id),
    FOREIGN KEY  (sensor_id) REFERENCES biota.sensors (id)
    	    	 ON DELETE CASCADE,
    FOREIGN KEY  (metric_id) REFERENCES biota.metrics (id)
    	    	 ON DELETE CASCADE
);

