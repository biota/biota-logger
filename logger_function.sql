--
-- SPDX-License-Identifier: GPL-3.0-or-later
--
-- biota/biota-logger/logger_functions.sql
-- Copyright (C) 2022 Fifth Estate
-- See biota:logger:db(8) for documentation
--
-- This program is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
-- 
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
-- details.
-- 
-- You should have received a copy of the GNU General Public License along with
-- this program.  If not, see <https://www.gnu.org/licenses/>.
--


-- Function to extract telemetry by sensor
CREATE OR REPLACE FUNCTION biota.list_telemetry_by_sensor(
    _sensor_id biota.sensors.id % TYPE,
    _timezone  TEXT
) RETURNS TABLE (
    id              biota.telemetry.id % TYPE,
    sensor_id       biota.sensors.id % TYPE,
    sensor_name     biota.sensors.name % TYPE,
    sensor_location biota.sensors.location % TYPE,
    metric_id       biota.metrics.id % TYPE,
    metric_name     biota.metrics.name % TYPE,
    metric_unit     biota.metrics.unit % TYPE,
    logged_at       TIMESTAMP
) LANGUAGE 'sql' AS
$$
    SELECT
        t.id,
	t.sensor_id,
	s.name,
	s.location,
	t.metric_id,
	m.name,
	m.unit,
	t.logged_at AT TIME ZONE _timezone
    FROM biota.telemetry AS t
    INNER JOIN biota.sensors AS s ON t.sensor_id = s.id
    INNER JOIN biota.metrics AS m ON t.metric_id = m.id
    WHERE t.sensor_id = _sensor_id
    ORDER BY logged_at DESC
$$;
